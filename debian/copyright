Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PixelMed Java DICOM Toolkit
Upstream-Contact: pixelmed_dicom@yahoogroups.com
Source: http://www.pixelmed.com/
Files-Excluded: com/pixelmed/display/testmammohome
                com/pixelmed/displaywave/PFE105.dat
                com/pixelmed/dicom/JIS0208Mapping.dat
                com/pixelmed/displaywave/PFE105_redred.scp
                com/pixelmed/displaywave/PFE105_high.scp

Files: *
Copyright: 2001-2025, David A. Clunie DBA PixelMed Publishing. All rights reserved.
License: BSD-3-clause

Files: apple/*
Copyright: 2003-2007 Apple, Inc.
License: AML

Files: debian/*
Copyright: 2009-2014 Mathieu Malaterre <malat@debian.org>
           2016-2017 Andreas Tille <tille@debian.org>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without modification, are
 permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this list of
    conditions and the following disclaimers.
 .
 2. Redistributions in binary form must reproduce the above copyright notice, this list of
    conditions and the following disclaimers in the documentation and/or other materials
    provided with the distribution.
 .
 3. Neither the name of PixelMed Publishing nor the names of its contributors may
    be used to endorse or promote products derived from this software.
 .
 This software is provided by the copyright holders and contributors "as is" and any
 express or implied warranties, including, but not limited to, the implied warranties
 of merchantability and fitness for a particular purpose are disclaimed. In no event
 shall the copyright owner or contributors be liable for any direct, indirect, incidental,
 special, exemplary, or consequential damages (including, but not limited to, procurement
 of substitute goods or services; loss of use, data or profits; or business interruption)
 however caused and on any theory of liability, whether in contract, strict liability, or
 tort (including negligence or otherwise) arising in any way out of the use of this software,
 even if advised of the possibility of such damage.
 .
 This software has neither been tested nor approved for clinical use or for incorporation in
 a medical device. It is the redistributor's or user's responsibility to comply with any
 applicable local, state, national or international regulations.

License: AML
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by
 Apple Inc. ("Apple") in consideration of your agreement to the
 following terms, and your use, installation, modification or
 redistribution of this Apple software constitutes acceptance of these
 terms.  If you do not agree with these terms, please do not use,
 install, modify or redistribute this Apple software.
 .
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc.
 may be used to endorse or promote products derived from the Apple
 Software without specific prior written permission from Apple.  Except
 as expressly stated in this notice, no other rights or licenses, express
 or implied, are granted by Apple herein, including but not limited to
 any patent rights that may be infringed by your derivative works or by
 other works in which the Apple Software may be incorporated.
 .
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 .
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
