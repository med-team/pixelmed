pixelmed (20250211+dfsg-1) unstable; urgency=medium

  * New upstream version
    Closes: #1084318
  * cme fix dpkg-control
  * Remove target which depends on files available in developers home

  [ Pierre Gruet ]
  * Ignoring iconv failure when converting encoding of some .properties files, in Greek among others
  * Stopping building the javadoc
  * Omitting html files with possible privacy breaches

 -- Andreas Tille <tille@debian.org>  Sun, 23 Feb 2025 22:17:24 +0100

pixelmed (20220618+dfsg-3) unstable; urgency=medium

  * Packaging update
  * Standards-Version: 4.7.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 16 Feb 2025 18:20:03 +0100

pixelmed (20220618+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Fixed the buid failure with Java 21 (Closes: #1053074)

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 18 Nov 2023 18:53:50 +0100

pixelmed (20220618+dfsg-1) unstable; urgency=medium

  * Team upload.
  * d/rules: Fixed typo in DEB_VERSION_UPSTREAM
  * New upstream version 20220618+dfsg
  * d/patches: Refresh patches
  * d/patches: Add support for opencsv in Debian archive
  * d/control: Bump Std-Vers to 4.6.1 no changes needed

 -- Mathieu Malaterre <malat@debian.org>  Fri, 24 Jun 2022 11:54:30 +0200

pixelmed (20220313+dfsg-1) unstable; urgency=medium

  * Team upload.
  * d/rules: Add TIFFToDicom command line tool
  * New upstream version 20220313+dfsg
  * d/manpages: Rework the manpages

 -- Mathieu Malaterre <malat@debian.org>  Wed, 16 Mar 2022 15:38:06 +0100

pixelmed (20210920+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 20210920+dfsg
  * Using downloadurlmangle in d/watch to correctly capture upstream tarballs
  * Removing d/compat file, depending on debhelper-compat 13
  * Raising Standards version to 4.6.0:
    - Rules-Requires-Root: no
    - Removing get-orig-source target, now handled by Files-Excluded
      in d/copyright
  * Refreshing patches
  * Adding missing jars in classpath for building
  * Skipping tests using non existing files
  * Adding jaxb-api.jar in the manifest
  * Adding a Lintian override for embedded-javascript-library
  * Using DEB_UPSTREAM_VERSION in d/rules instead of parsing changelog
  * Using secure uri in d/watch
  * Adding keywords in .desktop files
  * Forwarding patches

 -- Pierre Gruet <pgt@debian.org>  Tue, 21 Sep 2021 12:52:41 +0200

pixelmed (20200416-4) unstable; urgency=medium

  [ Pierre Gruet ]
  * Team upload.
  * Adapt Makefile after new libjsonp-java upstream version (Closes: #980604)
  * Refreshing d/copyright
  * Bumping Standards version to 4.5.1 (no changes needed)
  * Setting the classpath properly with /usr/share/java/ prefix
  * Adding /usr/share/java/javax.json-api.jar in classpath of the built jar

  [ Mathieu Malaterre ]
  * d/control: libjai-imageio-core-java is long gone

 -- Pierre Gruet <pgtdebian@free.fr>  Wed, 27 Jan 2021 08:31:46 +0100

pixelmed (20200416-3) unstable; urgency=medium

  * Team upload.
  * d/control: Add missing Depends on libbcprov-java
  * d/p/addmanifest.patch: Add missing ref to slf4j. Closes: #967936
  * d/p/dicomimageviewer: Add missing pixelmed_imageio jar
  * d/p/jpegblock.patch: Remove template cruft
  * d/p/add_usage.patch: Add missing help message for help2man
  * d/rules: Simplify help2man generation for version number
  * d/*1.: Refresh man pages
  * d/control Add missing pixelmed-imageio package

 -- Mathieu Malaterre <malat@debian.org>  Mon, 24 Aug 2020 10:34:32 +0200

pixelmed (20200416-2) unstable; urgency=medium

  * Team upload.
  * d/patches: Add -c flag to iconv to prevent failure. Closes: #966670

 -- Mathieu Malaterre <malat@debian.org>  Mon, 03 Aug 2020 08:54:27 +0200

pixelmed (20200416-1) unstable; urgency=medium

  * Team upload.
  * d/watch: Use upstream location
  * New upstream version 20200416
  * d/control: Run wrap-and-sort
  * d/rules: Make sure to clean file generated during test
  * d/control: Bump Std-Vers to 4.5.0 no changes needed
  * d/patches: Refresh patches

 -- Mathieu Malaterre <malat@debian.org>  Fri, 31 Jul 2020 17:11:56 +0200

pixelmed (20150917+git20151209.36f3174+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Removed the BUILDDATE file from pixelmed.jar to make the build reproducible
  * Allow running with any Java 7+ runtime
  * Use a unique short description for all packages
  * Standards-Version updated to 4.4.0

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 11 Sep 2019 13:20:51 +0200

pixelmed (20150917+git20151209.36f3174+dfsg-1) unstable; urgency=medium

  * New Github commit
    Closes: #893409
  * Point watch file to saved code copy on Github since upstream seems to
    be dead
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Remove trailing whitespace in debian/rules
  * Build-Depends: libjaxb-api-java
  * Do not fail if generating docs contains errors
  * Drop custom compression settings

  [ Olivier Sallou ]
  * Update classpath in javadoc to use debian dirs
  * Add vecmath in classpath for compilation
  * switch sun.misc.Cleaner to jdk.internal.ref.Cleaner (moved from JDK9)
    Unsure of impact, upstream should use dedicated library
    (java.lang.ref.Cleaner)

 -- Andreas Tille <tille@debian.org>  Thu, 20 Dec 2018 09:36:40 +0100

pixelmed (20150917+dfsg-1) unstable; urgency=medium

  [ Mathieu Malaterre ]
  * Remove self from Uploaders

  [ Andreas Tille ]
  * Move packaging from SVN to Git
  * Do not set bootclasspath
    Closes: #874150
  * debhelper 10
  * Standards-Version: 4.1.0
  * DEP3
  * d/rules: do not parse d/changelog
  * Fix DEP5 issues, update
  * Remove Encoding key from debian/*.desktop files

 -- Andreas Tille <tille@debian.org>  Fri, 15 Sep 2017 13:07:31 +0200

pixelmed (20150917-1) unstable; urgency=medium

  * New upstream version (Build-Depends: libpixelmed-codec-java)
  * Added myself to uploaders
  * cme fix dpkg-control
  * Add Build-Depends: libcommons-compress-java, libjsonp-java
  * Ignore failed build time tests

 -- Andreas Tille <tille@debian.org>  Wed, 13 Jan 2016 15:05:06 +0100

pixelmed (20140816-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Build-depend on libcommons-net-java (>= 3). (Closes: #800762)
  * debian/patches: Replace occurrences of commons-net2 with commons-net.

 -- Markus Koschany <apo@debian.org>  Sat, 23 Apr 2016 11:23:28 +0200

pixelmed (20140816-1) unstable; urgency=low

  * New upstream. Update dict to DICOM/2014

 -- Mathieu Malaterre <malat@debian.org>  Tue, 02 Sep 2014 18:39:38 +0200

pixelmed (20140429-1) unstable; urgency=low

  * New upstream. Remove invalid char from source code.

 -- Mathieu Malaterre <malat@debian.org>  Tue, 24 Jun 2014 21:50:23 +0200

pixelmed (20140326-1) unstable; urgency=low

  * New upstream
  * Add missing man pages
  * Bump Std-Vers to 3.9.5, no changes needed
  * Explicitly depends on java-wrappers. Closes: #742592

 -- Mathieu Malaterre <malat@debian.org>  Wed, 09 Apr 2014 22:36:42 +0200

pixelmed (20131018-1) unstable; urgency=low

  * New upstream. Closes: #728570, #717230
  * Remove patches applied usptream:
    - target_java_1.7.patch
    - encoding_issues.patch

 -- Mathieu Malaterre <malat@debian.org>  Tue, 10 Dec 2013 18:02:46 +0100

pixelmed (20130426-1) unstable; urgency=low

  * New upstream
    Split pixelmed-java into libpixelmed-java and pixelmed-apps
  * Provides webapps as command line utils. Closes: #657354
    See pixelmed-webstart-apps package
  * Add command line tools. Closes: #664087
    See pixelmed-apps package
  * FTBFS with Java7 (uses internal Java API). Closes: #678383
  * Switch to XZ compression

 -- Mathieu Malaterre <malat@debian.org>  Mon, 03 Jun 2013 09:33:37 +0200

pixelmed (20130220-1) experimental; urgency=low

  * New upstream
  * Add -doc package for documentation
  * Add -www package to provide JNLP apps

 -- Mathieu Malaterre <malat@debian.org>  Thu, 25 Apr 2013 14:32:48 +0200

pixelmed (20120508-1) unstable; urgency=low

  * New upstream
   - Better handling of float in DICOM SR

 -- Mathieu Malaterre <malat@debian.org>  Thu, 31 May 2012 12:04:54 +0000

pixelmed (20120405-1) unstable; urgency=low

  * New upstream
  * Use my @d.o alias
  * fix issue with encapsulated DICOM files (need libjai-imageio-core-java)
  * Add missing jmdns.jar in manifest file

 -- Mathieu Malaterre <malat@debian.org>  Tue, 08 May 2012 15:25:52 +0200

pixelmed (20120218-1) unstable; urgency=low

  * New upstream
   - Major code update to support locale other than english.
  * Bump Std-Vers to 3.9.3, no changes needed

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Mon, 05 Mar 2012 11:04:22 +0100

pixelmed (20111230-1) unstable; urgency=low

  * Fix Vcs URLs. Remove quilt for BD
  * New upstream
  * Fix OCR for GE machine

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Mon, 16 Jan 2012 10:21:21 +0100

pixelmed (20111205-1) unstable; urgency=low

  * New upstream
  * Add upstream Changelog
  * Add OCR support for Toshiba

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Tue, 06 Dec 2011 09:49:46 +0100

pixelmed (20110928-1) unstable; urgency=low

  * New upstream
  * remove ioexception.patch applied upstream

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Wed, 26 Oct 2011 15:58:36 +0200

pixelmed (20110901-1) unstable; urgency=low

  * New upstream
  * Switch to dch 8
  * Switch to dpkg-source 3.0 (quilt) format

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Tue, 20 Sep 2011 21:13:59 +0200

pixelmed (20110717-1) unstable; urgency=low

  * New upstream
  * libcommons-net2-java >= 2.2 to resolve org.apache.commons.net.ftp.FTPSClient
    Closes: #610296
  * Bump Standards-Version to 3.9.2 no changes needed.
  * Forces uses of JmDNS >= 3.4 since API changes from 3.1

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Fri, 19 Aug 2011 16:44:35 +0200

pixelmed (20101204-1) unstable; urgency=low

  * New upstream
  * Add dep to libcommons-net-java to resolve org.apache.commons.net.ftp.FTP
  * Bump Standards-Version to 3.9.1 no changes needed.

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Sun, 16 Jan 2011 18:24:49 +0100

pixelmed (20100617-1) unstable; urgency=low

  * New upstream

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Mon, 05 Jul 2010 20:13:41 +0200

pixelmed (20100430-1) unstable; urgency=low

  * New upstream
  * Fix keyword issues (2008 -> 2009 transition)

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Mon, 03 May 2010 17:54:47 +0200

pixelmed (20100217-1) unstable; urgency=low

  * New upstream.
  * Bump Standards-Version to 3.8.4 no changes needed.

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Wed, 24 Feb 2010 18:42:00 +0100

pixelmed (20091114-1) unstable; urgency=low

  * Initial release. (Closes: #547184)

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Sat, 21 Nov 2009 22:30:57 +0100
