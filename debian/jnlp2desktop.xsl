<?xml version="1.0" encoding="utf-8"?>
<!--
 Copyright (c) 2013 Mathieu Malaterre <malat@debian.org>

 XSLT scripts to convert an input JNLP file into an output .desktop file

Ref:
http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
http://standards.freedesktop.org/menu-spec/latest/apa.html
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text" indent="no" omit-xml-declaration="yes" encoding="utf-8"/>
  <xsl:strip-space elements="*"/>
  <xsl:param name="pixelmed_version"/>
  <xsl:template match="title">
    <xsl:text>Name=</xsl:text>
    <xsl:value-of select="normalize-space(.)"/>
    <xsl:text>
</xsl:text>
    <xsl:text>Exec=/usr/bin/</xsl:text>
    <xsl:value-of select="normalize-space(.)"/>
    <xsl:text>
</xsl:text>
  </xsl:template>
  <xsl:template match="description[@kind='short']">
    <xsl:text>Comment=</xsl:text>
    <xsl:value-of select="normalize-space(.)"/>
    <xsl:text>
</xsl:text>
  </xsl:template>
  <xsl:template match="icon">
    <xsl:text>Icon=/usr/share/pixelmed/</xsl:text>
    <xsl:value-of select="normalize-space(@href)"/>
    <xsl:text>
</xsl:text>
  </xsl:template>
  <xsl:template match="description">
    <xsl:text>GenericName=</xsl:text>
    <xsl:value-of select="normalize-space(.)"/>
    <xsl:text>
</xsl:text>
  </xsl:template>
  <xsl:template match="information">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="jnlp">
    <xsl:text>[Desktop Entry]
Encoding=UTF-8
Type=Application
Terminal=false
StartupNotify=true
Version=</xsl:text>
<xsl:value-of select="$pixelmed_version"/>
<xsl:text>
</xsl:text>
    <xsl:apply-templates/>
<xsl:text>Categories=Science;DataVisualization;ImageProcessing;MedicalSoftware
</xsl:text>
<!--
TryExec=
MimeType=application/x-;
-->
  </xsl:template>
  <xsl:template match="*|@*">
  </xsl:template>
</xsl:stylesheet>
